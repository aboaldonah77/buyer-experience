
# Running AB tests with `launch-darkly.vue`

## Background

When we say we want to run an AB test, this means running an experiment between two competing designs. One is called `control` or the existing design; Another is called `experiment` or the change in design. We want to measure the success of both of these variations against real users, and keep the winning variation. We also want to make sure this change didn't happen by chance, and that the difference is significant enough. For example, we want the "Get Free trial" button on the home page's hero to increase by 5%.  

To implement this experiment, we will need to define both at least one **metric** and exactly one **feature flag**. 

- A **feature flag** is used to decide what variation to show to which users. So far, we have simply done 50/50 splits, with our user base split in half. 
- A **metric** is an event that will fire based on the action that a user can take. This can be a button click, filling out a form, hovering over something, or viewing a certain part of the page. 

### Best practices for getting valid results

1. **Don't** use too many variations at a time.
2. **Don't** change experiment settings in the middle of the experiment
3. **Do** use tools like [this](https://vwo.com/tools/ab-test-duration-calculator/) to track how long you should run an experiment for  

## The `launch-darkly.vue` component 

This component is front-end wrapper to LaunchDarkly's SDK. On the client, it evaluates the value of a *feature flag* and displays the contents of either an `experiment` or `control`.

Go to the page Vue component that you want to run an experiment on, and do the following: 

```javascript
// ~/pages/enterprise.vue
import LaunchDarkly from '~/components/launch-darkly.vue'
```

```javascript
  components: {
    YourOtherComponent,
    LaunchDarkly
  },
```

```vue
<LaunchDarkly featureFlag="your-feature-flag-name-here"> 
  <template #experiment>
  // Your experiment goes here
  </template>
  <template #control>
  // Your control goes here
  </template>
</LaunchDarkly>
```

If you do not propely author the `control` and `experiments` slots, a warning will be thrown out to the `console`. You can force a page to show either the control/experiment by passing `?override-control` or `?ovveride-test` respectively as a URL parameter. 

## Launchdarkly Interface
Go into the Launchdarkly interface, navigate to the Buyer's Experience project, then create a *feature flag* here. You will need to attach a particular metric event to this *feature flag.* 

**NOTE** Something that was done differently compared to running experiments on `www` was setting dummy user key to `{ key: 'aa0ceb' }`. Because of this, we will need to serve feature flags by something other than `key`. We recommend you change that value in the feature flag to serve by `ip`. In the Targeting tag of the feature flag targeting rule, you should see an Advanced drop down.

Docs https://docs.launchdarkly.com/home/creating-experiments

We recommend creating metrics as custom conversions compared to using click events. This is because page redirects will happen before the click event can be sent back to LaunchDarkly.  We will have greater control and ensure that the metric event is captured prior to redirect. If we are sure that a click event will be captured 
## Limitations 

As our Buyer's Experience site is a purely statically generated site, we will need to make a client-side request to resolve the value of a *feature flag*. Without a server to offload that request, there will always be latency between the rest of the page and the contents of the experiment. This introduces a problem where we have to decide whether to flash content of a test on to the page, or display the contents of the entire page once the value of the *feature flag* is resolved. Either of these can negatively affect page speed metrics.

We are currently limited to ten seats, so you may need to reach out to either the managers of Digital Experience or [engineering leads](https://about.gitlab.com/handbook/marketing/digital-experience/#groups-metrics--team-members)

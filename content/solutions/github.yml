---
  title: GitLab CI/CD for GitHub
  description: With the GitHub integration, GitLab users can now create a CI/CD project in GitLab connected to an external GitHub.com or GitHub Enterprise code repository!
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab CI/CD for GitHub
        subtitle: Host your code on GitHub. Build, test, and deploy on GitLab.
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          url: https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/github_integration.html
          text: Documentation
          data_ga_name: github integration
          data_ga_location: header
        # TODO: Change for actual image
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          image_url_mobile: /nuxt-images/solutions/no-image-mobile.svg
          alt: "Image: gitLab  ci/cd for github"
    - name: copy
      data:
        block:
          - header: Automate build and test
            text: |
              With the GitHub integration, GitLab users can now create a CI/CD project in GitLab connected to an external GitHub.com or GitHub Enterprise code repository. This will automatically prompt GitLab CI/CD to run whenever code is pushed to GitHub and post CI/CD results back to both GitLab and GitHub when completed.
    - name: copy-media
      data:
        block:
          - header: Who is GitLab CI/CD for GitHub for?
            text: |
              ### Open source projects
              If you have a public, open source project on GitHub you can now take advantage of free CI/CD on GitLab&#46;com. As part of our commitment to open source, we offer all public projects our highest tier features (GitLab SaaS Ultimate) for free. While other CI/CD vendors limit you to running a handful of concurrent jobs, [GitLab.com](https://gitlab.com){data-ga-name="gitlab.com" data-ga-location="open source projects CI"} gives open source projects hundreds of concurrent jobs with 50,000 free CI pipeline minutes.

              ### Large Enterprises
              When we talk to our largest customers they tell us that they often have many teams using many different tools. They want to standardize on GitLab for CI/CD but code is stored in GitLab, GitHub, and other repos. This feature now allows enterprises to use common CI/CD pipelines across all of their different repos. This is a key audience and why we’ve made CI/CD for GitHub part of our self-managed Premium plan.

              ### Anyone using GitHub&#46;com
              While GitLab is designed to use SCM & CI/CD in the same application, we understand the appeal of using GitLab CI/CD with GitHub version control. So, for the next year we are making the GitLab CI/CD for GitHub feature a part of our [GitLab.com](https://gitlab.com){data-ga-name="gitlab.com" data-ga-location="anyone using github.com"} Free tier. That means anyone using GitHub from personal projects and startups to SMBs can use GitLab CI/CD for free. Starting at 400 free CI pipeline minutes, folks can also [add their own Runners](https://docs.gitlab.com/ee/ci/runners/README.html#registering-a-specific-runner){data-ga-name="add runners" data-ga-location="body"} or upgrade plans to get more.

              ### Gemnasium customers
              We recently [acquired Gemnasium](/press/releases/2018-01-30-gemnasium-acquisition.html){data-ga-name="gemnasium" data-ga-location="body"}. While we are super excited about having such a great team join our ranks, we also want to take care of folks that were using Gemnasium and provide them a migration path. We’ve already [shipped Gemnasium features](/blog/2018/02/22/gitlab-10-5-released/#gemnasium-dependency-checks){data-ga-name="gemnasium features" data-ga-location="body"} as part of our built-in security scanning. Now, GitLab CI/CD for GitHub allows Gemnasium customers that were using GitHub + Gemnasium to begin using GitLab CI/CD for their security needs without needing to migrate their code.
            image:
              image_url: /nuxt-images/logos/github-logo.svg
              alt: ""
          - header: Benefits
            inverted: true
            text: |
              With GitLab CI/CD for GitHub, users can create a CI/CD project in GitLab connected to an external GitHub code repository. This will automatically configure several components:

              * [Pull mirroring](https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html#pulling-from-a-remote-repository){data-ga-name="pull mirroring" data-ga-location="body"} of the repository.
              * A push webhook to GitLab triggers CI/CD immediately once a code is committed.
              * GitHub project service integration webhooks CI status back to GitHub.
            image:
              image_url: /nuxt-images/features/github-status-github.png
              alt: ""
          - header: GitLab CI/CD for External Repositories
            text: |
              Not only does GitLab integrate with GitHub, but you can also run CI/CD from any external git repo from any vendor by adding a repo by URL to your project and configuring webhook. For example, you can [configure Bitbucket to use GitLab CI/CD](https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/bitbucket_integration.html){data-ga-name="bitbucket for gitlab" data-ga-location="body"}.

              Read the documentation for [GitLab CI/CD for external repositories](https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/){data-ga-name="ci/cd for external repositories" data-ga-location="body"}.
            image:
              image_url: /nuxt-images/logos/git-logo.svg
              alt: ""
          - header: Plans and Pricing
            inverted: true
            text: |
              GitLab CI/CD for GitHub is not priced separately, but comes bundled as a feature of GitLab's standard end-to-end product.

              For **self-managed installations**, GitLab CI/CD for GitHub is available for customers with **Premium** and **Ultimate** license plans.

              GitLab CI/CD for GitHub will be available promotionally in our **Free** tier through March, 22, 2020. (After March 22, 2020, this feature will move to the **Premium** tier and be available for users on **Premium** and **Ultimate**.)

              Learn more about GitLab subscription options visit the [pricing page](/pricing/){data-ga-name="pricing" data-ga-location="body"}.
            icon:
              name: checklist
              alt: Checklist
              variant: marketing
              hex_color: '#F43012'
          - header: Learn more
            text: |
              * GitLab CI/CD: Learn more about the [benefits of GitLab CI/CD](/features/continuous-integration/){data-ga-name="CI/CD" data-ga-location="body"}.
              * Docs: Get started with the [documentation](https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/github_integration.html){data-ga-name="documentation" data-ga-location="body"}.
              * Release: Read the release post for [GitLab 10.6](/blog/2018/03/22/gitlab-10-6-released/){data-ga-name="gitlab 10.6" data-ga-location="body"}.
            image:
              image_url: /nuxt-images/logos/gitlab-logo.svg
              alt: ""

---
  title: GitLab for Open Source
  description: Open source communities benefit from the one DevOps platform.
  image_title: /nuxt-images/open-graph/open-source-card.png
  image_alt: GitLab for Open Source
  twitter_image: https://about.gitlab.com/nuxt-images/open-graph/open-source-card.png
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab Solutions for Open Source Projects
        subtitle: Create. Configure. Monitor. Secure. Open source communities benefit from the one DevOps platform.
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          url: /solutions/open-source/join/
          text: Join the program
          data_ga_name: join open source program
          data_ga_location: header
        secondary_btn:
          url: https://forum.gitlab.com/c/community/gitlab-for-open-source/
          text: Ask a question
          data_ga_name: ask an open source question
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          image_url_mobile: /nuxt-images/solutions/no-image-mobile.svg
          alt: "Image: gitlab for open source"
    - name: benefits
      data:
        header: Why GitLab for open source?
        cards_per_row: 2
        text_align: left
        aos_animation: fade-up
        aos_duration: 800
        benefits:
          - icon:
              name: branch-circle
              alt: Branch Icon
              variant: marketing
              hex_color: '#64A7F0'
            title: We're open
            description: |
              GitLab's [open core](https://gitlab.com/gitlab-org) is published under an MIT open source license. The rest is source-available. [Everyone can contribute](/community/contribute/) to making GitLab better. View our [transparent roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=start_date_asc&layout=WEEKS&timeframe_range_type=CURRENT_QUARTER&progress=WEIGHT&show_progress=true&show_milestones=true&milestones_type=ALL) and propose features your project needs.
          - icon:
              name: agile-alt-2
              alt: Agile Icon
              variant: marketing
              hex_color: '#FA7035'
            title: You're in control
            description: |
              Download, install, and manage [your own GitLab instance](/install/?version=ce). Or [let us](/pricing/)—or [a partner](https://partners.gitlab.com/English/directory/)—do it for you. Deploy to the cloud you prefer. With GitLab, you have [options](/install/ce-or-ee/).
          - icon:
              name: collaboration-link
              alt: Collaboration Link Icon
              variant: marketing
              hex_color: '#FCA326'
            title: Community powered
            description: |
              Connect with open source enthusiasts on the [GitLab Forum](https://forum.gitlab.com/c/community/gitlab-for-open-source/) to find support and collaborate. And meet members of the [GitLab Open Source Partners](/solutions/open-source/partners/) program to learn how large-scale open source projects innovate with GitLab.
          - icon:
              name: source-code
              alt: Source Code Icon
              variant: marketing
              hex_color: '#1EBA9E'
            title: Built for collaboration
            description: |
              Your entire community can use GitLab—not only developers. Onboard new members with ease. Foster cross-team collaboration. Maintain your documentation. Plan new features and track upcoming milestones. Automate testing. GitLab's [end-to-end platform](/stages-devops-lifecycle/) helps everyone contribute.
    - name: pull-quote
      data:
        aos_animation: fade-up
        aos_duration: 800
        quote: Adopting GitLab has been a natural next step for us. Being able to allow project contributors to easily participate in how the products they maintain are tested and delivered will certainly be a turning point for our ecosystem.
        source: Aleix Pol 
        cite: KDE e.V
        hide_horizontal_rule: true
        shadow: true
    - name: copy-media
      data:
        block:
          - header: Get started
            miscellaneous: |
              Unlock your community's full potential. Qualifying open source projects receive free subscriptions to [GitLab Ultimate](/pricing/ultimate/) and 50,000 CI/CD minutes.
            media_link_href: /solutions/open-source/join/
            media_link_data_ga_name: join open source program
            media_link_data_ga_location: body
            media_link_text: Join the GitLab for Open Source Program
            metadata:
              id_tag: get-started-open-source
            hide_horizontal_rule: true
    - name: logo-links
      data:
        header: Open Source Partners
        clickable: true
        column_size: 3
        aos_animation: fade-up
        aos_duration: 800
        button:
          text: Meet our open source partners
          href: /solutions/open-source/partners/
          data_ga_name: meet our open source partners
          data_ga_location: body
        logos:
          - href: https://www.skatelescope.org/
            logo_url: /nuxt-images/organizations/logo_ska-partner_color.svg
            logo_alt: Ska Telescope
          - href: https://www.gnome.org/
            logo_url: /nuxt-images/organizations/logo_gnome_color.svg
            logo_alt: Gnome
          - href: https://www.debian.org/
            logo_url: /nuxt-images/organizations/logo_debian_color.svg
            logo_alt: Debian
          - href: https://www.kde.org/
            logo_url: /nuxt-images/organizations/logo_kde_color.svg
            logo_alt: KDE
          - href: https://www.drupal.org/
            logo_url: /nuxt-images/organizations/logo_drupal_color.svg
            logo_alt: Drupal
          - href: https://www.freedesktop.org/
            logo_url: /nuxt-images/organizations/logo_freedesktop_color.svg
            logo_alt: Free Desktop
          - href: https://www.x.org/
            logo_url: /nuxt-images/organizations/logo_xorg_color.svg
            logo_alt: X Org
          - href: https://www.xfce.org/
            logo_url: /nuxt-images/organizations/logo_xfce_color.svg
            logo_alt: XFCE
          - href: https://www.finos.org/
            logo_url: /nuxt-images/organizations/logo_finos_color.svg
            logo_alt: Finos
          - href: https://opencores.org/
            logo_url: /nuxt-images/organizations/logo_opencores_color.svg
            logo_alt: Open Cores
          - href: http://www.videolan.org/
            logo_url: /nuxt-images/organizations/logo_vlc_color.svg
            logo_alt: VLC
          - href: https://inkscape.org/
            logo_url: /nuxt-images/organizations/logo_inkscape_color.svg
            logo_alt: Inkscape
          - href: https://developer.arm.com/tools-and-software/open-source-software
            logo_url: /nuxt-images/organizations/logo_arm_color.svg
            logo_alt: ARM
          - href: https://www.kali.org/
            logo_url: /nuxt-images/organizations/logo_kali_color.svg
            logo_alt: Kali
          - href: https://www.haskell.org/
            logo_url: /nuxt-images/organizations/logo_haskell_color.svg
            logo_alt: Haskell
          - href: https://www.synchrotron-soleil.fr/en
            logo_url: /nuxt-images/organizations/logo_soleil_color.svg
            logo_alt: Soleil
          - href: https://archlinux.org/
            logo_url: /nuxt-images/organizations/logo_archlinux_color.svg
            logo_alt: Arch Linux
          - href: https://www.f-droid.org/
            logo_url: /nuxt-images/organizations/logo_fdroid_color.svg
            logo_alt: F-Droid
          - href: https://getfedora.org/
            logo_url: /nuxt-images/organizations/logo_fedora_color.svg
            logo_alt: Fedora
          - href: https://www.centos.org/
            logo_url: /nuxt-images/organizations/logo_centos_color.svg
            logo_alt: CentOS
    - name: pull-quote
      data:
        aos_animation: fade-up
        aos_duration: 800
        quote: The ability to contribute without setting up complex local development environments has made it easier for everyone from our accessibility maintainers, documentation editors, product managers, and others to review and comment on the work of our developers.
        source: Timothy Lehnen 
        cite: Drupal Association
        hide_horizontal_rule: true
        shadow: true
    - name: case-studies
      data:
        title: Read partner case studies
        has_horizontal_rule: true
        metadata:
          id_tag: open-source-partner-case-studies
        case_studies:
          - title: Drupal Association eases entry for new committers, speeds implementations
            author: by GitLab
            link_text: Read more
            image_url: /nuxt-images/organizations/logo_drupal_color.svg
            image_alt: Drupal Logo
            link_url: /customers/drupalassociation
            data_ga_name: Drupal
            data_ga_location: body
          - title: How SKA uses GitLab to help construct the world’s largest telescope
            author: by GitLab
            link_text: Read more
            image_url: /nuxt-images/organizations/logo_ska-partner_color.svg
            image_alt: SKAO Logo
            link_url: /customers/square_kilometre_array
            data_ga_name: SKAO
            data_ga_location: body
          - title: GitLab accelerates innovation and improves efficiency for Synchrotron SOLEIL
            author: by GitLab
            link_text: Read more
            image_url: /nuxt-images/organizations/logo_soleil_color.svg
            image_alt: SOLEIL Logo
            link_url: /customers/synchrotron_soleil
            data_ga_name: SOLEIL
            data_ga_location: body

---
  title: Continuous Software Security
  description: Integrating security into your DevOps lifecycle is easy with GitLab. Security and compliance are built in, out of the box, giving you the visibility and control necessary to protect the integrity of your software.
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        title: Continuous Software Security
        subtitle: Shift security left with built-in DevSecOps
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Start your free trial
          url: /free-trial/
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          image_url_mobile: /nuxt-images/solutions/no-image-mobile.svg
          alt: "Image: gitLab for continuous software security"
    - name: 'by-solution-intro'
      data:
        text:
          highlight: Integrating security into your DevOps lifecycle is easy with GitLab.
          description: Security and compliance are built in, out of the box, giving you the visibility and control necessary to protect the integrity of your software.
    - name: 'by-solution-benefits'
      data:
        title: Security. Compliance. Built-in.
        image:
          image_url: "/nuxt-images/solutions/benefits/solutions-computer.jpg"
          alt: GitLab for Automated Software Delivery
        is_accordion: true
        items:
          - icon:
              name: devsecops
              alt: DevSecOps Icon
              variant: marketing
            header: Integrated testing and remediation
            text: With every code commit, GitLab provides actionable security and compliance [findings to developers](https://docs.gitlab.com/ee/user/application_security/) to shift remediation earlier in the lifecycle while developers are still working on the code.
          - icon:
              name: continuous-integration
              alt: Continuous Integration Icon
              variant: marketing
            header: Manage software vulnerabilities
            text: While helping security pros [manage remaining vulnerabilities](https://docs.gitlab.com/ee/user/application_security/security_dashboard/#gitlab-security-dashboards-and-security-center) through resolution.
          - icon:
              name: cloud-tick
              alt: Continuous Integration Icon
              variant: marketing
            header: Cloud Native Application security
            text: GitLab helps you secure your cloud native applications and the infrastructure upon which they depend including containers, infrastructure-as-code, and APIs.
          - icon:
              name: automated-code
              alt: Automated Code Icon
              variant: marketing
            header: Guardrails and policy automation
            text: GitLab’s compliant pipelines, MR approvals, end-to-end transparency of audit events, along with built-in [common controls](https://docs.gitlab.com/ee/administration/compliance.html) help you secure your software supply chain and meet your [compliance needs](https://about.gitlab.com/solutions/compliance/).
    - name: 'by-solution-value-prop'
      data:
        title: Unleash developer to run fast - and secure
        cards:
          - title: Simplicity
            description: One platform, one price, with comprehensive application security.
            list:
              - text: <a href="https://docs.gitlab.com/ee/user/application_security/" data-ga-name="application security testing" data-ga-location="body">Application Security Testing</a>
              - text: <a href="https://docs.gitlab.com/ee/user/application_security/vulnerability_report/" data-ga-name="vulnerability management" data-ga-location="body">Vulnerability Management</a>
              - text: <a href="https://docs.gitlab.com/ee/user/application_security/cluster_image_scanning/" data-ga-name="deployed images" data-ga-location="body">Scan Deployed Images</a>
            icon:
              name: agile-alt
              alt: Agile Icon
              variant: marketing
          - title: Visibility
            description: See who changed what, where, when, end-to-end.
            list:
              - text: <a href="https://docs.gitlab.com/ee/administration/audit_events.html" data-ga-name="audit events" data-ga-location="body">Audit Events</a>
              - text: <a href="https://docs.gitlab.com/ee/administration/audit_reports.html" data-ga-name="audit reports" data-ga-location="body">Audit Reports</a>
              - text: <a href="https://docs.gitlab.com/ee/user/application_security/dependency_list/#dependency-list" data-ga-name="dependency list" data-ga-location="body">Dependency List (BOM)</a>
            icon:
              name: magnifying-glass-code
              alt: Magnifying Glass Code Icon
              variant: marketing
          - title: Control
            description: Compliance framework for consistency, common controls, policy automation.
            list:
              - text: <a href="https://docs.gitlab.com/ee/administration/compliance.html" data-ga-name="compliance capabilites" data-ga-location="body">Common compliance controls</a>
              - text: <a href="https://docs.gitlab.com/ee/user/application_security/configuration/" data-ga-name="security policy configuration" data-ga-location="body">Security Policy Configuration</a>
              - text: <a href="https://docs.gitlab.com/ee/user/project/settings/index.html#compliance-pipeline-configuration" data-ga-name="compliant pipelines" data-ga-location="body">Compliant pipelines</a>
            icon:
              name: less-risk
              alt: Less Risk Icon
              variant: marketing
    - name: 'home-solutions-container'
      data:
        title: DevOps platform security
        image: /nuxt-images/home/solutions/solutions-top-down.png
        alt: "Top down image of office"
        description: Visit our trust center to see how we secure the [GitLab software](https://about.gitlab.com/security/) and comply with industry standards.
        white_bg: true
        animation_type: fade
        solutions:
          - title: Test within the CI pipeline
            description: Use your scanners or ours. Shift security left to empower developers to find and fix security flaws as they are created. Comprehensive scanners include SAST, DAST, Secrets, dependencies, containers, IaC, APIs, cluster images, and  fuzz testing.
            link_text: Learn more
            link_url: https://docs.gitlab.com/ee/user/application_security/
            data_ga_name: Test CI pipeline
            data_ga_location: body
            icon:
              name: pipeline-alt
              alt: Pipeline Icon
              variant: marketing
            image: /nuxt-images/solutions/continuous-software-security/Continuous_Software_Security_-_1.png
            alt: "Text bubbles of communicating teams"
          - title: Assess dependencies
            description: Scan dependencies and containers for security flaws. Inventory dependencies used.
            icon:
              name: visibility
              alt: Visibility Icon
              variant: marketing
            image: /nuxt-images/solutions/continuous-software-security/Continuous_Software_Security_-_2.png
            alt: "Text bubbles of communicating teams"
          - title: Secure cloud native apps
            description: Test the security of cloud native elements such as infrastructure-as-code, APIs, and cluster images.
            icon:
              name: cloud-tick
              alt: Cloud Tick Icon
              variant: marketing
            image: /nuxt-images/solutions/continuous-software-security/Continuous_Software_Security_-_3.png
            alt: "Text bubbles of communicating teams"
          - title: Manage vulnerabilities
            description: Built for the security pro to vet, triage, and manage software vulnerabilities from pipelines, on-demand scans, third parties, and bug bounties all in one place. Immediate visibility as vulnerabilities are merged. Collaborate more easily on their resolution
            icon:
              name: continuous-integration
              alt: Continuous Integration Icon
              variant: marketing
            image: /nuxt-images/solutions/continuous-software-security/Continuous_Software_Security_-_4.png
            alt: "Text bubbles of communicating teams"
          - title: Secure your software supply chain
            description: Automate security and compliance policies across your software development lifecycle. Compliant pipelines ensure pipeline policies are not circumvented, while common controls provide end-to-end guardrails.
            icon:
              name: shield-check
              alt: Shield Check Icon
              variant: marketing
            image: /nuxt-images/solutions/continuous-software-security/Continuous_Software_Security_-_5.png
            alt: "Text bubbles of communicating teams"
    - name: 'tier-block'
      data:
        header: Which tier is right for you?
        tiers:
          - id: free
            title: Free
            items:
              - Static application security testing (SAST) and, secrets detection.
              - Findings in json file.
            link:
              href: /pricing/
              text: Get Started
              data_ga_name: pricing
              data_ga_location: free tier
              aria_label: free tier
          - id: premium
            title: Premium
            items:
              - Static application security testing (SAST) and, secrets detection.
              - Findings in json file.
              - MR approvals and more common controls
            link:
              href: /pricing/
              text: Learn more about pricing
              data_ga_name: pricing
              data_ga_location: premium tier
              aria_label: premium tier
          - id: ultimate
            title: Ultimate
            items:
              - Comprehensive security scanners include SAST, DAST, Secrets, dependencies, containers, IaC, APIs, cluster images, and fuzz testing
              - Findings in json file.
              - Actionable results within the MR pipeline
              - Compliance pipelines
              - Security and Compliance dashboards
              - Much more
            link:
              href: /pricing/
              text: Learn more about pricing
              data_ga_name: pricing
              data_ga_location: ultimate tier
              aria_label: ultimate tier
    - name: 'by-industry-case-studies'
      data:
        title: Customer Realized Benefits
        charcoal_bg: true
        rows:
          - title: HackerOne
            subtitle: HackerOne achieves 5x faster deployments with GitLab’s integrated security
            image:
              url: https://about.gitlab.com/images/blogimages/hackerone-cover-photo.jpg
              alt: Computer with code
            button:
              href: /customers/hackerone/
              text: Learn more
              data_ga_name: learn more
              data_ga_location: body
          - title: The Zebra
            subtitle: How The Zebra achieved secure pipelines in black and white
            image:
              url: https://about.gitlab.com/images/blogimages/thezebra_cover.jpg
              alt: Mobile phone taking picture of car
            button:
              href: /customers/thezebra/
              text: Learn more
              data_ga_name: learn more
              data_ga_location: body
          - title: Hilti
            subtitle: How CI/CD and robust security scanning accelerated Hilti’s SDLC
            image:
              url: https://about.gitlab.com/images/blogimages/hilti_cover_image.jpg
              alt: Building sky scrapers
            button:
              href: /customers/hilti/
              text: Learn more
              data_ga_name: learn more
              data_ga_location: body
    - name: 'solutions-resource-cards'
      data:
        column_size: 4
        cards:
          - icon:
              name: webcast
              alt: Webcast Icon
              variant: marketing
            event_type: Video
            header: DevSecOps overview demo
            link_text: Watch now
            fallback_image: /nuxt-images/resources/fallback/img-fallback-cards-devops.png
            href: https://youtu.be/2mmw3SF7Few
            aos_animation: fade-up
            aos_duration: 400
          - icon:
              name: webcast
              alt: Webcast Icon
              variant: marketing
            event_type: Video
            header: Learn how to add Security to your CICD Pipeline
            link_text: Watch now
            image: /nuxt-images/features/resources/resources_webcast.png
            href: https://youtu.be/Fd5DhebtScg
            aos_animation: fade-up
            aos_duration: 600
          - icon:
              name: webcast
              alt: Webcast Icon
              variant: marketing
            event_type: Video
            header: Efficiently manage vulnerabilities and risk using the GitLab Security Dashboards
            link_text: Watch now
            fallback_image: /nuxt-images/resources/fallback/img-fallback-cards-infinity.png
            href: https://youtu.be/p3qt2z1rQk8
            aos_animation: fade-up
            aos_duration: 800
          - icon:
              name: webcast
              alt: Webcast Icon
              variant: marketing
            event_type: Video
            header: Manage your Application Dependencies
            link_text: Watch now
            image: /nuxt-images/features/resources/resources_waves.png
            href: https://youtu.be/scNS4UuPvLI
            aos_animation: fade-up
            aos_duration: 1000


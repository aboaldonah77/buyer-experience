---
title: Support Portal
description: Support Portal
support-hero:
  data:
    title: Support Portal
side_menu:
  anchors:
    text: 'ON THIS PAGE'
    data:
      - text: 'First time reaching support?'
        href: '#first-time-reaching-support'
        nodes:
          - text: 'Setting up an account on support.gitlab.com'
            href: '#setting-up-an-account-on-supportgitlabcom'                                  
      - text: "Having trouble with the Support Portal?"
        href: "#having-trouble-with-the-support-portal"
        nodes:
          - text: "Language Support"
            href: "#language-support"                                       
  hyperlinks:
    text: ''
    data: []
components:
- name: support-copy
  data:
    block:
    - header: First time reaching support?
      id: first-time-reaching-support
      text: |
        <p>
          If you just purchased a license, there's a few things to do to make sure the folks maintaining your GitLab get the smoothest support experience possible.
        </p>
        <ol>
          <li>
            <a href="#setting-up-an-account-on-supportgitlabcom">Set up an account</a> on <a href="https://support.gitlab.com">support.gitlab.com</a>
          </li>
          <li>
            Create your first ticket <a href="/support/managing-support-contacts/#getting-set-up">to register your company's authorized support contacts</a>.
            <ul>
              <li>(Optional) Set up a <a href="/support/managing-support-contacts/#shared-organizations">shared organization</a> so that your authorized support contacts can see each others tickets.</li>
            </ul>
          </li>
          <li>
            Just in case someone is left out, circulate <a href="/support/managing-support-contacts/#proving-your-support-entitlement">instructions on proving your support entitlement</a> within your company.
          </li>
          <li>
            Learn about <a href="/support/#working-effectively-in-support-tickets">how to work effectively in support tickets</a>.
          </li>
          <li>
            Familiarize yourself with our <a href="/support/statement-of-support">Statement of Support</a> to understand the scope of what to expect in your interactions.
          </li>
          <li>
           Take a look at <a href="/support/#priority-support">what's included in Priority Support</a> and the 
           <a href="/support/#effect-on-support-hours-if-a-preferred-region-for-support-is-chosen">effect choosing a support region has on tickets</a> 
           to understand the time frame in which you can expect a first response.
          </li>
        </ol>
    - subtitle:
        text: Setting up an account on support.gitlab.com
        id: setting-up-an-account-on-supportgitlabcom
      text: |
        <p>Account setup can happen one of two ways:</p>
        <ol>
          <li>
            Go to support.gitlab.com and submit a new request. An account and password will be created for you. You will need to request a password reset and setup a new password before you can sign in.
          </li>
          <liClick on Sign Up to create a new account using your company email address.></liClick>
        </ol>
        <p>
          You can keep track of all of your tickets and their current status using the GitLab Support Portal! We recommend using the Support Portal for a superior experience managing your tickets. 
          To learn more about using the Support Portal, watch <a href="https://www.youtube.com/watch?v=NQzkGD7nIqQ">this video on using <strong<Zendesk as an end user</strong></a>.
        </p>
    - header: Having trouble with the Support Portal?
      id: having-trouble-with-the-support-portal
      text: |
        <p>Occasionally, you may find the <a href="https://support.gitlab.com/">Support Portal</a> not acting as expected. This is often caused by the user’s setup. When encountering this, the recommended course of action is:</p>
        <ol>
          <li>
            Ensure your browser is allowing third party cookies. These are often vital for the system to work. A general list to allow would be:
            <ul>
              <li><code>[*.]zendesk.com</code></li>
              <li><code>[*.]zdassets.com</code></li>
              <li><code>[*.]gitlab.com</code></li>
            </ul>
          </li>
          <li>
            Disable all plugins/extensions/addons on the browser.
          </li>
          <li>
            Disable any themes on the browser.
          </li>          
          <li>
            Clear all cookies and cache on the browser.
          </li>         
          <li>
            Try logging in again to the the Support Portal.
          </li>          
          <li>
            If you are still having issues, write down the browser’s version, type, distro, and other identifying information
          </li>          
          <li>
            Generate a HAR file (process will vary from browser to browser) and send this to support. If you are unable to create a ticket, then communicate with your Technical Account Manager and/or Account Manager. 
            The next best place to send HAR file and browser information is via a GitLab.com issue.
          </li>
        </ol>
    - subtitle:
        text: Language Support
        id: language-support
      text: |
        <p>
          Ticket support is available in the following languages:
        </p>
        <ul>
          <li>Chinese</li>
          <li>English</li>
          <li>French</li>
          <li>German</li>
          <li>Japanese</li>
          <li>Korean</li>
          <li>Portuguese</li>
          <li>Spanish</li>
        </ul>
        <p>
          While we do not currently provide translated interfaces for our <a href="https://support.gitlab.com/">Support Portal</a>, you can write in your preferred language and we will respond accordingly.
        </p>
        <br />
        <p>
          Should you be offered a call, only English is available.
        </p>
        <br />
        <p>
          NOTE:
        </p>
        <p>
          Any attached media used for ticket resolution must be sent in English.
        </p>


---
title: Provide large files to GitLab support
description: How to share large files with the GitLab support team.
support-hero:
  data:
    title: Provide large files to GitLab support
    content: | 
      Zendesk has a <a href="https://support.zendesk.com/hc/en-us/articles/235860287-What-is-the-maximum-attachment-size-I-can-include-in-ticket-comments-">maximum attachment size</a> of 20MB per file. 
      Zendesk will not allow us to increase this limit any further.
side_menu:
  anchors:
    text: 'ON THIS PAGE'
    data:
      - text: 'Support Uploader'
        href: '#support-uploader'
      - text: "Other methods"
        href: "#other-methods"
        nodes:
          - text: 'Compression'
            href: '#compression'
          - text: 'File sharing services'
            href: '#file-sharing-services'
          - text: 'GitLab private project'
            href: '#gitlab-private-project'
          - text: "Use GNU split"
            href: "#use-gnu-split"                                      
  hyperlinks:
    text: ''
    data: []
components:
- name: support-copy
  data:
    block:
    - header: Support Uploader
      id: support-uploader
      text: |
        <p>
          A Support Engineer can use the <a href="https://gitlab.com/gitlab-com/support/support-uploader">Support Uploader</a> to generate a zip 
          bundle for a ticket where communication is ongoing with you, if you request this in the ticket.
        </p>
        <br />
        <p>
          You will find attached in the ticket a <code>gs_uploader_<ticketID>.zip</code> bundle which includes two files, 
          an HTML and a SH file, you can use either one to upload large files to us:
        </p>
        <br />
        <p>
          You can either open the HTML file in your browser and attach the file using the browser form, or run <code>bash gs_upload.sh /path/to/filename</code>.
        </p>
        <br />
        <p>
          The upload uses a pre-signed S3 URL with write-only access to a directory limited to this ticket ID, 
          where only GitLab Support team members have access to read.
        </p>
        <br />
        <p>
          Please feel free to inspect the code before using it, and also note that it is 
          <a href="https://gitlab.com/gitlab-com/support/support-uploader">open source</a>, 
          you can also analyze the code in the project and find a more comprehensive explanation of how it works.
        </p>
        <br />
        <p>
          The upload currently has a 3 GiB limit per file and will expire in approximately 24 hours from the time it is generated, after which you will not be able to use 
          it to upload other files - if you need more time, please let us know in the ticket and we can generate another one.
        </p>
    - header: Other methods
      id: other-methods
      text: |
        <p>
          For legacy reasons, we also list methods that were used in the past to provide large files to us. If you absolutely cannot use the 
          Support Uploader, you can choose one of the following to share your files.
        </p>
    - subtitle:
        text: Compression
        id: compression
      text: |
        <p>
          If you're sending a text file or an archive with mostly text files, then please compress it. Use either bz2 (preferred) or gzip 
          (faster) compression and it should compress to a small percentage of its original size. Zip compression is fine if you're on Windows.
        </p>
        <br />
        <p>
          If this brings your file under the 20MB limit, then simply attach it to the ticket and be done. If not, then see below for more options.
        </p>
    - subtitle:
        text: File sharing services
        id: file-sharing-services
      text: |
        <p>
          Please feel free to use your own choice of file sharing service. Be aware that submitting potentially sensitive data to 3rd 
          parties does carry a risk, so be sure to check with your security team for a properly vetted choice.
        </p>
    - subtitle:
        text: GitLab private project 
        id: gitlab-private-project
      text: |
        <p>
          This is a fairly straight-forward option. <a href="https://gitlab.com/users/sign_in#register-pane">Sign up for a gitlab.com account</a> 
          if you don't already have one. Then create a private project and invite the Support Engineer(s) assisting you with <strong>Reporter</strong> access or higher. 
          You can find the GitLab handle of the support engineer(s) you need to add by searching for the respective engineer(s) on our <a href="/company/team/">team</a> page.
        </p>
    - subtitle:
        text: Use GNU split
        id: use-gnu-split
      text: |
        <p>
          Since the attachment is applied per file, we can split that one file into many and attach all of them to a ticket.
        </p>
        <br />
        <p>
          The <code>split</code> command is bundled in GNU coreutils, which should be installed on all Unix-like operating systems by default. 
          Please avoid using alternatives like winzip, winrar, 7zip, etc. We've included an example below:
        </p>
        <br />
        <blockquote>
          <code>split -b <size> <source file> <prefix for new files></code>
          <br />
          <code>split -b 19M source-file.tar.bz2 "target-file.tar.bz2."s</code>
        </blockquote>
        <br />
        <p>
          This will create many files in your current directory such as <code>target-file.tar.bz2.aa</code> and <code>target-file.tar.bz2.ab.</code> These files can be later joined with the <code>cat</code> command.
        </p>
        <br />
        <blockquote>
          <code>cat target-file.tar.bz2.* > joined-file.tar.bz2</code>
        </blockquote>
